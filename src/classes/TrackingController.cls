/*	@description: Tracking Controller class to handle all the server 
 * 				  side processing of Tracking Lightning Component.
 * 	@date: 2016-11-10
 * 	@developer: Saquib S.
*/
public without sharing class TrackingController {
    
    final static String strQuery = ('SELECT Id, Name, CreatedDate, Asset__r.Account.Shop__c,' + 
                                    ' Position__c, Position__r.Short_Description__c, FIlled__c,' + 
                                    'X__c, X0__c, X25__c, X50__c, X75__c, X100__c FROM Tracking__c');
    
    /*	@description: Get per page size from TrackingLCPageSize__c Custom Setting records.
    */
    @AuraEnabled 
    public static Integer getPageSize() {
        Integer pageSize = 0;
        TrackingLCPageSize__c trackingPageSize = TrackingLCPageSize__c.getValues('TrackingPageSize');
        if(trackingPageSize != NULL && trackingPageSize.PageSize__c != NULL) {
            pageSize = Integer.valueOf(trackingPageSize.PageSize__c);
        }
        else if(trackingPageSize != NULL && trackingPageSize.PageSize__c == NULL) {
            trackingPageSize = TrackingLCPageSize__c.getValues('TrackingPageSize');
            trackingPageSize.PageSize__c = 25;
            try {
                UPDATE trackingPageSize;
            }
            catch(Exception e) {
                System.debug('Error : ' + e.getMessage());
            }
        }
        else {
            trackingPageSize = new TrackingLCPageSize__c();
            trackingPageSize.Name = 'TrackingPageSize';
            trackingPageSize.PageSize__c = 25;
            try {
                INSERT trackingPageSize;
            }
            catch(Exception e) {
                System.debug('Error : ' + e.getMessage());
            }
            pageSize = 25;
        }
        return pageSize;
    }
    
    /*	@description: Get list of Tracking object records based on Period.
     * 	@param: Selected Period on component.
    */
    @AuraEnabled
    public static List<TrackingWrapper> getTrackingRecordsOnPeriod(String period) {
        System.debug('period : ' + period); 
        List<Tracking__c> listTrackings;
        listTrackings = Database.query(strQuery + ' WHERE CreatedDate = ' + period + ' ORDER BY CreatedDate DESC LIMIT 50000');       
           
        if(listTrackings != NULL && !listTrackings.isEmpty())
        	return createTrackingWrapperRecords(listTrackings);
        return (new List<TrackingWrapper>());
    }
    
    /*	@description: Get list of Tracking object records based on CreateDate 
     * 				  in the range of start datetime and end datetime.
     *	@param: Start datetime and end datetime for filtering 
	 *			Tracking records based on CreatedDate.
	*/
    @AuraEnabled
    public static List<TrackingWrapper> getTrackingRecordsOnDatetime(String startDatetime, String endDatetime) {
        System.debug('startDatetime : ' + startDatetime);
        System.debug('endDatetime : ' + endDatetime);
        List<Tracking__c> listTrackings;           
        listTrackings = Database.query(strQuery + ' WHERE CreatedDate >= ' + startDatetime + 
                                       ' AND CreatedDate <= ' + endDatetime + ' ORDER BY CreatedDate DESC LIMIT 50000');        
  
        if(listTrackings != NULL && !listTrackings.isEmpty())
        	return createTrackingWrapperRecords(listTrackings);
        return (new List<TrackingWrapper>());
    }    
    
    /*	@description: Create list of TrackingWrapper records based on Tracking object 
     * 				  records and its related Position and Asset.Account records.
     *	@param: List of Tracking object records.
	*/
    private static List<TrackingWrapper> createTrackingWrapperRecords(List<Tracking__c> listTrackings) {        
        List<TrackingWrapper> listTrackingWrapper = new List<TrackingWrapper>();
        
        if(listTrackings != NULL && !listTrackings.isEmpty()) {
            Set<Id> setPositionIds = new Set<Id>();    
            Set<Id> setTrackingIds = new Set<Id>();  
           
            for(Tracking__c tracking : listTrackings) {
                setTrackingIds.add(tracking.Id);
                setPositionIds.add(tracking.Position__c);
            }
            
            Map<Id, Id> mapTrackingIdNVerId = new Map<Id, Id>();            
            if(!setTrackingIds.isEmpty())
                mapTrackingIdNVerId = createMapEntityIdNVerId(setTrackingIds);
            
            Map<Id, Id> mapPositionIdNVerId = new Map<Id, Id>();
            if(!setPositionIds.isEmpty())
                mapPositionIdNVerId = createMapEntityIdNVerId(setPositionIds);  
            
            TrackingWrapper trackingWrapper;
            for(Tracking__c tracking : listTrackings) {
                trackingWrapper = new TrackingWrapper();
                trackingWrapper.recordId = tracking.Id;
                trackingWrapper.createdDate = tracking.CreatedDate;
                trackingWrapper.shop = tracking.Asset__r.Account.Shop__c;
                trackingWrapper.shortDescription = tracking.Position__r.Short_Description__c;
                trackingWrapper.filled = Integer.valueOf(tracking.FIlled__c);
                trackingWrapper.X = tracking.X__c;
                trackingWrapper.X0 = tracking.X0__c;
                trackingWrapper.X25 = tracking.X25__c;
                trackingWrapper.X50 = tracking.X50__c;
                trackingWrapper.X75 = tracking.X75__c;
                trackingWrapper.X100 = tracking.X100__c;
                if(mapTrackingIdNVerId != NULL && !mapTrackingIdNVerId.isEmpty())
                    trackingWrapper.filePhotoId = mapTrackingIdNVerId.containsKey(tracking.Id) ? mapTrackingIdNVerId.get(tracking.Id) : NULL;
                if(mapPositionIdNVerId != NULL && !mapPositionIdNVerId.isEmpty())
                    trackingWrapper.calibratedPhotoId = mapPositionIdNVerId.containsKey(tracking.Position__c) ? mapPositionIdNVerId.get(tracking.Position__c) : NULL;
                listTrackingWrapper.add(trackingWrapper);
            }
        }
        System.debug('listTrackingWrapper : ' + listTrackingWrapper);
        return listTrackingWrapper;
    }    
    
    /*	@description: Create map of EntityId(SobjectId or UserId) and its related Content Version Id.
     * 	@param: Set of SObjectIds(Tracking records Id)
	*/
    private static Map<Id, Id> createMapEntityIdNVerId(Set<Id> setSobjectIds) {          
        System.debug('setSobjectIds : ' + setSobjectIds);
        if(setSobjectIds != NULL && !setSobjectIds.isEmpty()) {
            List<ContentDocumentLink> listContentDocLinks = new List<ContentDocumentLink>();
            listContentDocLinks = [SELECT Id, LinkedEntityId, ContentDocumentId 
                                   FROM ContentDocumentLink 
                                   WHERE LinkedEntityId IN :setSobjectIds LIMIT 50000];
            System.debug('listContentDocLinks : ' + listContentDocLinks);
            
            Set<Id> setContentDocIds = new Set<Id>();
            for(ContentDocumentLink docLink : listContentDocLinks) {
                setContentDocIds.add(docLink.ContentDocumentId);
            }
            System.debug('setContentDocIds : ' + setContentDocIds);
            
            if(setContentDocIds != NULL && !setContentDocIds.isEmpty()) {
                List<ContentVersion> listContentVers = new List<ContentVersion>();
                listContentVers = [SELECT Id, ContentDocumentId 
                                   FROM ContentVersion
                                   WHERE ContentDocumentId IN :setContentDocIds];
                System.debug('listContentVers : ' + listContentVers);            
                
                Map<Id, Id> mapDocIdNVerId = new Map<Id, Id>();
                for(ContentVersion contentVer : listContentVers) {
                    mapDocIdNVerId.put(contentVer.ContentDocumentId, contentVer.Id);
                }
                System.debug('mapDocIdNVerId : ' + mapDocIdNVerId);
                
                Map<Id, Id> mapEntityIdNDocId = new Map<Id, Id>();
                for(ContentDocumentLink docLink : listContentDocLinks) {
                    if(mapDocIdNVerId.containsKey(docLink.ContentDocumentId)) {
                        mapEntityIdNDocId.put(docLink.LinkedEntityId, mapDocIdNVerId.get(docLink.ContentDocumentId));
                    }
                }
                System.debug('mapEntityIdNDocId : ' + mapEntityIdNDocId);
                return mapEntityIdNDocId;
            }
        }        
        return (new Map<Id, Id>());
    }
    
    /*	@description: Update the Tracking records Filled percent.
     * 	@param: List of Strings, each string contains the Tracking record Id 
     * 			and its Filled percentage update values(both seperated by ";")
	*/
    @AuraEnabled
    public static String updateTrackingRecords(List<String> listRecordIdsNVal) {
        List<Tracking__c> listTrackings = new List<Tracking__c>();
        Tracking__c tracking;
        String recordId;
        Integer filledVal;
        
        if(listRecordIdsNVal != NULL && !listRecordIdsNVal.isEmpty()) {
            for(String recordIdNVal : listRecordIdsNVal) {
                if(String.isNotBlank(recordIdNVal) && recordIdNVal.split(';').size() == 2) {
                    recordId = recordIdNVal.split(';')[0];
                    filledVal = Integer.valueOf(recordIdNVal.split(';')[1] == 'X' ? '-1' : recordIdNVal.split(';')[1]);
                    
                    tracking = new Tracking__c();
                    tracking.Id = recordId;
                    tracking.FIlled__c = filledVal;
                    tracking.X__c = (filledVal == -1 ? true : false);
                    tracking.X0__c = (filledVal == 0 ? true : false);
                    tracking.X25__c = (filledVal == 25 ? true : false);
                    tracking.X50__c = (filledVal == 50 ? true : false);
                    tracking.X75__c = (filledVal == 75 ? true : false);
                    tracking.X100__c = (filledVal == 100 ? true : false);            
                    listTrackings.add(tracking);
                }
            }
            
            if(listTrackings != NULL && !listTrackings.isEmpty()) {
                try {
                    UPDATE listTrackings;
                }
                catch(Exception e) {
                    System.debug('Error: ' + e.getLineNumber() + ' - ' + e.getMessage());
                    return ('Error: ' + e.getLineNumber() + ' - ' + e.getMessage());
                }
            }
        }
        else {
            return ('Error: No records for update');
        }
        return ('Records updated successfully');
    }
    
    public class TrackingWrapper {
        @AuraEnabled public Id recordId {get; set;}
        @AuraEnabled public Datetime createdDate {get; set;}
        @AuraEnabled public String shop {get; set;}
        @AuraEnabled public String shortDescription {get; set;}
        @AuraEnabled public Integer filled {get; set;}
        @AuraEnabled public Boolean X {get; set;}
        @AuraEnabled public Boolean X0 {get; set;}
        @AuraEnabled public Boolean X25 {get; set;}
        @AuraEnabled public Boolean X50 {get; set;}
        @AuraEnabled public Boolean X75 {get; set;}
        @AuraEnabled public Boolean X100 {get; set;}
        @AuraEnabled public Id filePhotoId {get; set;}
        @AuraEnabled public Id calibratedPhotoId {get; set;}  
    }
}