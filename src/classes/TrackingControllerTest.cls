/*
@ Description :TrackingControllerTest class used to Check the functionality of TrackingController class .
@ created Date : 21/11/2016
*/

@isTest
public class TrackingControllerTest {
    
    @testSetup
    public static void createTestData() {
        
        //Create Account record
        Account accountRecord = new Account(Name='TestAccount' , Shop__c='TestShop');
        
        INSERT accountRecord ;
        
        //Create Contact record
        Contact contactRecord = new Contact();
        contactRecord.FirstName='Bruce';
        contactRecord.LastName='Wayne';
        contactRecord.Accountid= accountRecord.id;
        
        INSERT contactRecord ;
        
        //Create Asset record
        Asset assetRecord = new Asset() ;
        assetRecord.Name = 'TestAsset' ;
        assetRecord.AccountId = accountRecord.Id ;
        assetRecord.ContactId = contactRecord.Id ;
        
        INSERT assetRecord ;
        
        //Create Position record.
        Position__c positionRecord = new Position__c() ;
        positionRecord.Name = 'TestPosition';
        positionRecord.Short_description__c = 'This is Test short description';
        
        INSERT positionRecord ;
        
        //Insert bulk Tracking__c records
        List<Tracking__c> trackingList = new List<Tracking__c>();
        for (Integer index = 1 ; index <= 210 ; index++) {
            if(math.mod(index,2) == 0){
                trackingList.add(new Tracking__c(Asset__c = assetRecord.Id ,
                                                 Position__c = positionRecord.Id ,
                                                 FIlled__c = 50 ,
                                                 X50__c = true) );
            }
            else{
                trackingList.add(new Tracking__c(Asset__c = assetRecord.Id ,
                                                 Position__c = positionRecord.Id ,
                                                 FIlled__c = 75 ,
                                                 
                                                 X75__c = true) );
            }
        }
        
        INSERT trackingList ;
        
        //create ContentVersion record 
        List<ContentVersion> contentVersionList = new  List<ContentVersion>();
        for (Integer index = 1 ; index <= 205 ; index++) {
            contentVersionList.add( new ContentVersion(Title =  'Test contentVersion' + index ,
                                                       PathOnClient = 'path' + index,
                                                       VersionData =  Blob.valueOf('This is long test string' + index) ) );
            
        }
        INSERT contentVersionList ;
        
        // Create ContentDocumentLink records
        List<ContentDocumentLink> listContentDocumentLink = new List<ContentDocumentLink>();
        List<ContentVersion> contentVersionRecords  = [Select Id,ContentDocumentId from ContentVersion ];
        
        for (Integer index = 1 ; index <= 204 ; index++) {
            Tracking__c trackingRecord = trackingList[index] ;
            
            listContentDocumentLink.add(new ContentDocumentLink(LinkedEntityId = trackingRecord.Id ,
                                                                ContentDocumentId = contentVersionRecords[index].ContentDocumentId , 
                                                                ShareType='V'));
        }
        
        INSERT listContentDocumentLink ;
    }
    
    
    // Test getTrackingRecordsOnPeriod() method 
    public static testMethod void testGetTrackingRecordsOnPeriod() {
        
        Test.startTest();
        TrackingController.getPageSize();
        TrackingController.getTrackingRecordsOnPeriod('TODAY');
        List<Tracking__c> trackingRecordList = [SELECT Id, Name FROM Tracking__c WHERE CreatedDate = TODAY];
        system.assertEquals(trackingRecordList.size(), 210) ;
        DateTime dt = DateTime.now();
        String formattedDt = dt.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        TrackingController.getTrackingRecordsOnDatetime(formattedDt , formattedDt);
        system.assertEquals(trackingRecordList.size(), 210) ;
    }
    
    
    //Test updateTrackingRecords() method
    @isTest 
    public static void testUpdateTrackingRecords() {
        TrackingLCPageSize__c trackingPageSize = new TrackingLCPageSize__c();
        trackingPageSize.Name = 'TrackingPageSize';
        trackingPageSize.PageSize__c = 25;
        INSERT trackingPageSize;
        
        Test.startTest();       
        TrackingController.getPageSize();        
        String strQuery = ('SELECT Id, Name FROM Tracking__c');
        
        List<Tracking__c> listTrackingRecords = Database.query(strQuery);
        List<String> listTrackingToUpdate = new List<String>();
        
        system.assertEquals(listTrackingRecords.size(), 210) ;
        
        for(Tracking__c trackingRecord : listTrackingRecords) {
            String trackingValue ;
            trackingValue = '' + trackingRecord.Id + ';' + '100' ;
            listTrackingToUpdate.add(trackingValue);
        }
        
        TrackingController.updateTrackingRecords(listTrackingToUpdate);
        
        system.assertEquals(listTrackingRecords.size(), 210) ;
        
        Test.stopTest();
    }
    
}