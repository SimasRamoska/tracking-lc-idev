({
    afterScriptLoad : function(component) {
        var self=this;       
        
        $(function() {  
            //console.log("Doc Ready Method");
            $(".manualTimeframe").hide();           
            self.colSortEvent(component, self);        
            self.filledPercentChange(component, self);  
            
            $(".slds-is-sortable:first").addClass("slds-has-focus");
            
            $(".filterOption").on("click", function() {
                console.log("click event on radio filter");
                 $(".filterOption").removeAttr("checked");
                $(".filterOption").removeAttr("defaultchecked");
                $(".filterOption").removeProp("checked");	
                $(this).prop("checked");
                $(this).attr("checked","checked");
            });
            
            $("thead th").click(function() {
                $("th").find(".slds-th__action").removeClass("slds-has-focus");
                $(this).find(".slds-th__action").addClass("slds-has-focus");
            });  
        });
    },
    
    filterOtionChangeEvent: function(component, event, helper, IsPeriod, IsManualDatetime) { 
        if(IsPeriod) {
            $(".filter").removeClass("slds-hide");
            $(".period").removeClass("slds-hide");
            $(".period").addClass("slds-show"); 
            if($(window).width() <= 1024 && $(window).width() >= 481) {
                $(".period").css("display","block");
            }
            else {
                $(".period").css("display","inline-block");
            }                
            $(".manualTimeframe").addClass("slds-hide");
            $(".manualTimeframe").removeClass("slds-show"); 
            $(".manualTimeframe").css("display","none");
        }
        else {
            $(".filter").removeClass("slds-hide");
            $(".period").addClass("slds-hide");
            $(".period").removeClass("slds-show"); 
            $(".period").css("display","none");
            $(".manualTimeframe").removeClass("slds-hide");
            $(".manualTimeframe").addClass("slds-show"); 
            $(".manualTimeframe").css("display","inline-block");
        }   
    },
    
    colSortEvent: function(component, self) {
        
        $(".slds-is-sortable").click(function() {     
            var lastSortedCol = component.get('v.lastSortedColMap');
            //console.log("before lastSortedCol : " + lastSortedCol);     
            
            $("th").removeClass("slds-has-focus");
            $(this).addClass("slds-has-focus");
            var colName = $(this).closest("th").attr("aria-label");
            $(".slds-icon").removeClass("toggle");
            $(this).find(".slds-icon").addClass("toggle");
            $(".sortDescIcon").addClass("slds-hide");
            $(".sortAscIcon").addClass("slds-hide");
            
            if(lastSortedCol[colName] != undefined && lastSortedCol[colName] != "") {
                //console.log("if");
                if(lastSortedCol[colName] == "asc") {
                    $(this).find(".sortAscIcon").addClass("slds-hide");
                    $(this).find(".sortDescIcon").removeClass("slds-hide");
                    lastSortedCol[colName] = "desc";
                    self.sortTrackings(component, colName, lastSortedCol[colName]);
                }
                else if(lastSortedCol[colName] == "desc") {
                    $(this).find(".sortAscIcon").removeClass("slds-hide");
                    $(this).find(".sortDescIcon").addClass("slds-hide");
                    lastSortedCol[colName] = "asc";
                    self.sortTrackings(component, colName, lastSortedCol[colName]);
                }    
            }
            else {
                //console.log("else");
                $(this).find(".sortDescIcon").removeClass("slds-hide");
                lastSortedCol[colName] = "desc";   
                self.sortTrackings(component, colName, lastSortedCol[colName]);
            }  
            component.set("v.lastSortedColMap", lastSortedCol);
            //console.log("colName : " + colName);
            //console.log("lastSortedCol[colName] : " + lastSortedCol[colName]);
            //console.log("lastSortedCol : " + JSON.stringify(lastSortedCol));
        });
    },
    
    sortTrackings: function(component, colName, sortOrder) {
        var trackingRecords = component.get("v.trackingWrappers");
        //console.log("before sort trackingRecords : " + JSON.stringify(trackingRecords));
        
        trackingRecords.sort(function(a,b) {
            if(colName == "Created Date") {
                var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                if(sortOrder == "asc")			return dateA-dateB;
                else if(sortOrder == "desc")	return dateB-dateA;
            }
            else if(colName == "Location") {
                var shopA = (a.shop != undefined ? a.shop.toLowerCase() : "");
                var shopB = (b.shop != undefined ? b.shop.toLowerCase() : "");
                if(sortOrder == "asc") {
                    if (shopA < shopB)	return -1; 
                    else if (shopA > shopB)	return 1;
                    else { 
                        var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                        if(sortOrder == "asc")			return dateA-dateB;                        
                    }
                }
                else if(sortOrder == "desc") {
                    if (shopA > shopB)	return -1; 
                    else if (shopA < shopB)	return 1;
                    else { 
                        var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                        if(sortOrder == "desc")	return dateB-dateA;                       
                    }
                }  
            }
            else if(colName == "Position") {
                var shortDescriptionA = (a.shortDescription != undefined ? a.shortDescription.toLowerCase() : ""); 
                var shortDescriptionB = (b.shortDescription != undefined ? b.shortDescription.toLowerCase() : "");
                if(sortOrder == "asc") {
                    if (shortDescriptionA < shortDescriptionB)	return -1; 
                    else if (shortDescriptionA > shortDescriptionB)	return 1;
                    else { 
                        var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                        if(sortOrder == "asc")			return dateA-dateB;                        
                    }
                }
                else if(sortOrder == "desc") {
                    if (shortDescriptionA > shortDescriptionB)	return -1; 
                    else if (shortDescriptionA < shortDescriptionB)	return 1;
                    else { 
                        var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                        if(sortOrder == "desc")	return dateB-dateA;                       
                    }
                }
            }
            else if(colName == "Filled%") {
                var filledA = (a.filled != undefined ? a.filled : 0);
                var filledB = (b.filled != undefined ? b.filled : 0);
                if(sortOrder == "asc") {
                	if((filledA - filledB) != 0)	return filledA - filledB;
                    else {
                        var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                        if(sortOrder == "asc")			return dateA-dateB;      
                    }
                }
                else if(sortOrder == "desc"){
                	if((filledB - filledA) != 0)	return filledB - filledA;       
                    else {
                        var dateA = new Date(a.createdDate), dateB = new Date(b.createdDate);
                        if(sortOrder == "desc")	return dateB-dateA; 
                    }
                }
            }
        });     
        
        component.set("v.trackingWrappers", trackingRecords);
        //console.log("sortTrackings trackingWrappers : " + JSON.stringify(trackingRecords));
        this.changeListIndex(component, component.get("v.counter"));
    },
    
    filledPercentChange: function(component, self) {
        /*$("input:radio[class=filledPercent]").on("change", function() {
            console.log("filledPercent changed");
        });*/
    },
    
    getPageSize: function(component) {
        var action = component.get("c.getPageSize"); 
        action.setCallback(this, function(response){ 
            var state = response.getState();
            //console.log('state : ' + state);
            if (state === "SUCCESS") {
                //console.log('pageSize : ' + response.getReturnValue());
                component.set("v.pageSize", response.getReturnValue());
            }
        });       
        $A.enqueueAction(action);
    },
    
    getTrackingRecordsOnPeriod: function(component, period, IsUpdate) {      
        var action = component.get("c.getTrackingRecordsOnPeriod");
        action.setParams({ period : period });
        
        action.setCallback(this, function(response){ 
            this.trackingsCallback(component, response, IsUpdate);
        });        
        $A.enqueueAction(action);
    },
    
    getTrackingRecordsOnDatetime: function(component, startDatetime, endDatetime, IsUpdate) {       
        var action = component.get("c.getTrackingRecordsOnDatetime");
        action.setParams({ startDatetime : startDatetime,
                          endDatetime : endDatetime});       
        
        action.setCallback(this, function(response){ 
            this.trackingsCallback(component, response, IsUpdate);
        });       
        $A.enqueueAction(action);
    },
    
    trackingsCallback: function(component, response, IsUpdate) {
        var state = response.getState();
        //console.log('state : ' + state);
        if (state === "SUCCESS") {
            
            component.set("v.trackingWrappers", response.getReturnValue());
            if(response.getReturnValue().length > 0) {
                component.set("v.trackingWrappers", response.getReturnValue());
                var trackingWrappersList = response.getReturnValue();
                var pageSize = component.get("v.pageSize");
                var trackings = new Array();
                for(var i=0; (i < pageSize && i < trackingWrappersList.length); i++) {
                    trackings.push(trackingWrappersList[i]);
                }
                
                if(!IsUpdate) {
                    component.set("v.counter", "0");
                    //component.set("v.currentPageNumber", "1");
                    component.set("v.totalPages", Math.ceil(trackingWrappersList.length/pageSize));
                    component.set("v.totalListSize", trackingWrappersList.length);
                    //component.set("v.trackings", trackings);
                    
                } else {
                    //component.set("v.trackings", trackings);
                    //this.changeListIndex(component, component.get("v.counter"));
                }
                //console.log("trackings : " + JSON.stringify(trackings));
                //this.changeListIndex(component, component.get("v.counter"));
                //console.log("trackingsCallback trackingWrappersList : " + JSON.stringify(trackingWrappersList));
                this.retainPreviousSortCol(component);
                
            } else {
                component.set("v.counter", "0");
                component.set("v.currentPageNumber", "0");
                component.set("v.totalPages", "0");
                component.set("v.totalListSize", "0");
                this.showToast(component, "No records found.", "info"); 
                component.set("v.trackings", response.getReturnValue());
            }
            
        } else if (state === "INCOMPLETE") {
           
        } else if (state === "ERROR") {
            var errors = response.getError();
            if (errors) {
                if (errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                                errors[0].message);
                }
            } else {
                console.log("Unknown error");
            }
        }
    },
    
    updateTrackingRecords: function(component, recordIdsNVal) {
        var action = component.get("c.updateTrackingRecords");
        action.setParams({ listRecordIdsNVal : recordIdsNVal });
        
        action.setCallback(this, function(response){ 
            this.trackingsUpdateCallback(component, response);
        });        
        $A.enqueueAction(action);
    },
    
    trackingsUpdateCallback: function(component, response) {
        var state = response.getState();
        //console.log("trackingsUpdateCallback state : " + state);
        if(state === "SUCCESS") {      
            this.showToast(component, response.getReturnValue(), "success");   
            this.rerenderTrackingRecords(component);
        
        } else if(state === "INCOMPLETE") {
            
        } else if(state === "ERROR") {
            var errors = response.getError();
            if(errors) {
                if(errors[0] && errors[0].message) {
                    console.log("Error message: " + 
                                errors[0].message);
                }
            } 
            else{
                console.log("Unknown error");
            }
        }
    },
    
    changeListIndex: function(component, counter) {              
        var trackingWrappersList = component.get("v.trackingWrappers");
        var pageSize = component.get("v.pageSize");
        var trackings = [];  
        counter /= 1;
        var totalListSize = component.get("v.totalListSize"); 
        var totalPages = component.get("v.totalPages");
        var currentPageNumber = component.get("v.currentPageNumber") / 1;        
        
        for(var i = counter; (i < (counter + pageSize) && i < totalListSize); i++) {
            trackings.push(trackingWrappersList[i]);
        }   
                
        component.set("v.counter", counter);     
        component.set("v.currentPageNumber", ((counter/pageSize) + 1));
        //component.set("v.totalListSize", totalListSize);
        //component.set("v.pageSize", pageSize);
        component.set("v.trackings", trackings);         
        //console.log("changeListIndex : " + counter + " ---- " + pageSize + " ---- " +  totalPages + " ---- " +  totalListSize + " ---- " +  component.get("v.currentPageNumber"));
        //console.log("changeListIndex trackingWrappersList : " + JSON.stringify(trackingWrappersList));
        //console.log("changeListIndex trackings : " + JSON.stringify(trackings));
        //this.retainPreviousSortCol(component);
    },
    
    retainPreviousSortCol: function(component) {
        var self = this;
        console.log("retainPreviousSortCol");
        $(".slds-is-sortable").each(function() {
            if($(this).hasClass("slds-has-focus")) {
                var colName = $(this).closest("th").attr("aria-label");
                var lastSortCol = component.get("{!v.lastSortedColMap}");
                var colSortOrder = lastSortCol[colName];
                //console.log("colName : " + colName);
                //console.log("lastSortCol : " + JSON.stringify(lastSortCol));
                //console.log("colSortOrder : " + colSortOrder);
                
                $(".slds-icon").removeClass("toggle");
                $(this).find(".slds-icon").addClass("toggle");
                $(".sortDescIcon").addClass("slds-hide");
                $(".sortAscIcon").addClass("slds-hide");
                
                if(colSortOrder === "asc") {
                    $(this).find(".sortAscIcon").removeClass("slds-hide");
                    $(this).find(".sortDescIcon").addClass("slds-hide");
                    //lastSortCol[colName] = "desc";
                    self.sortTrackings(component, colName, "asc");
                }
                else if(colSortOrder === "desc") {
                    $(this).find(".sortAscIcon").addClass("slds-hide");
                    $(this).find(".sortDescIcon").removeClass("slds-hide");
                    //lastSortCol[colName] = "asc";
                    self.sortTrackings(component, colName, "desc");
                }
                //console.log("lastSortCol : " + JSON.stringify(lastSortCol));
                component.set("{!v.lastSortedColMap}", lastSortCol);
                //console.log("retainPreviousSortCol slds-has-focus tag : " + ($(this).attr("aria-label")));
             	//return true;
            }
        });
    },
    
    showToast : function(component, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "type": type, //The toast type, which can be error, warning, success, or info. The default is other, which is styled like an info toast and doesn’t display an icon, unless specified by the key attribute. 
            "message": message
        });
        toastEvent.fire();
        
        if(type == "success") {
            //var previous = $(".filterOption input[type='radio']:checked").attr("id");
            //$A.get('e.force:refreshView').fire();            
            //$("#" + previous).prop("checked", true);     
        }
    },
    
    rerenderTrackingRecords: function(component) {
        var counter = component.get("v.counter");
        if($(".period").css("display") !== "none") {
            var selectedVal = component.find("period");
            var period = selectedVal.get("v.value");
            //console.log(period);
            this.getTrackingRecordsOnPeriod(component, period, true);
            component.set("v.counter", counter)         
        }
        else {
            var startDatetime = component.find("startDatetime").get("v.value");
            var endDatetime = component.find("endDatetime").get("v.value");
            //console.log("startDatetime : " + startDatetime);
            //console.log("endDatetime : " + endDatetime);             
            this.getTrackingRecordsOnDatetime(component, startDatetime, endDatetime, true);
        }
    },
    
    showSpinner : function (component) {
        //console.log("show spinner");
        var spinner = component.find("waitSpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner : function (component) {
        //console.log("hide spinner");
        var spinner = component.find("waitSpinner");
        $A.util.addClass(spinner, "slds-hide");
    }    
})