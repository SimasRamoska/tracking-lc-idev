({
    doInit : function(component, event, helper) {
        var self=this;
        //helper.showSpinner(component);
        helper.getPageSize(component);        
       // helper.getTrackingRecordsOnPeriod(component, "TODAY");
    },
    
    afterScriptLoad : function(component, event, helper) {
        //helper.showSpinner(component, event, helper);
        helper.afterScriptLoad(component);
        //$A.get('e.force:refreshView').fire(); 
    },
    
    filterChange: function(component, event, helper) {
        var IsPeriod = $("#period").prop("checked");
        var IsManualDatetime = $("#manualDatetime").prop("checked");
        //console.log("Filter Change Event IsPeriod : " + IsPeriod);
        //console.log("Filter Change Event IsManualDatetime : " + IsManualDatetime);
        if(IsPeriod) {
            //$("#period").find(".slds-radio--button__label").css("background-color", "rgb(0, 112, 210) !important");
            //$("#manualTimeframe").find(".slds-radio--button__label").css("background-color", "transparent !important");
            $(".period").removeClass("slds-hide");
            $(".period").addClass("slds-show"); 
            if($(window).width() <= 1024 && $(window).width() >= 481) {
                $(".period").css("display","block");
            }
            else {
                $(".period").css("display","inline-block");
            }                
            $(".manualTimeframe").addClass("slds-hide");
            $(".manualTimeframe").removeClass("slds-show"); 
            $(".manualTimeframe").css("display","none");
        }
        else {     
            //$("#manualTimeframe").find(".slds-radio--button__label:first").css("background-color", "rgb(0, 112, 210) !important");
            //$("#period").find(".slds-radio--button__label").css("background-color", "transparent !important");
            $(".period").addClass("slds-hide");
            $(".period").removeClass("slds-show"); 
            $(".period").css("display","none");
            $(".manualTimeframe").removeClass("slds-hide");
            $(".manualTimeframe").addClass("slds-show"); 
            $(".manualTimeframe").css("display","inline-block");
        }   
    },
    
    periodSubmit : function(component, event, helper) {        
        var selectedVal = component.find("period");
        var period = selectedVal.get("v.value");
        //console.log(period);
        helper.getTrackingRecordsOnPeriod(component, period);
    },
    
    manualDatetimeSubmit : function(component, event, helper) {        
        var startDatetime = component.find("startDatetime").get("v.value");
        var endDatetime = component.find("endDatetime").get("v.value");
        
        //console.log("startDatetime : " + startDatetime);
		//console.log("endDatetime : " + endDatetime);  
        /*if(Object.prototype.toString.call(startDatetime) === '[object Date]') {
            alert("true");
            helper.showToast(component, "Please enter valid datetime.", "error");
        }*/
        if(startDatetime == undefined || endDatetime == undefined || startDatetime == '' || endDatetime == '') {
            helper.showToast(component, "Please enter valid datetime.", "error");
        }
        else {
        	helper.getTrackingRecordsOnDatetime(component, startDatetime, endDatetime);
        }        
    },
    
    filledChangeEvent: function(component, event, helper) {        
        var targetId = event.target.id;
        jQuery("#" + targetId).closest("td").addClass("IsChanged");
        //console.log("filledPercent changed : " + event.target.id);
        //console.log("filledPercent td class : " + $("#" + targetId).closest("td").attr("class"));        
    },
    
    saveRecords: function(component, event, helper) {    
        var recordIdsNVal = new Array();
        var recordId;
        var selectedVal;
        
        jQuery(".IsChanged").each(function() {
            recordId = jQuery(this).find("input:radio:checked").attr("name");
            selectedVal = jQuery(this).find("input:radio:checked").attr("data-value");
            recordIdsNVal.push(recordId + ";" + selectedVal);
            //console.log(recordId + " --> " + selectedVal);
        });
        //console.log("recordIdsNVal : " + JSON.stringify(recordIdsNVal));
        if(recordIdsNVal == undefined || recordIdsNVal.length == 0) 
            helper.showToast(component, "No records for update", "warning");
        else
        	helper.updateTrackingRecords(component, recordIdsNVal);
    },
    
    first: function(component, event, helper) {
        helper.changeListIndex(component, 0);
    },
    
    previous: function(component, event, helper) {
        var counter = component.get("v.counter");
        var pageSize = component.get("v.pageSize");
        counter = counter - pageSize;
        console.log("previous : " + counter + " ---- " + pageSize);
        helper.changeListIndex(component, counter);
    },
    
    next: function(component, event, helper) {
        var counter = component.get("v.counter");
        var pageSize = component.get("v.pageSize");
        counter = counter + pageSize;        
        console.log("next : " + counter + " ---- " + pageSize);
        helper.changeListIndex(component, counter);
    },
    
    last: function(component, event, helper) {
        var counter = component.get("v.counter");
        var pageSize = component.get("v.pageSize");
        var totalListSize = component.get("v.totalListSize");
        
        if((totalListSize % pageSize) > 0)
            counter = totalListSize - (totalListSize % pageSize);
        else if(totalListSize > pageSize){                
            counter = ((totalListSize/pageSize) - 1) * pageSize;
        }
        helper.changeListIndex(component, counter);
    },
    
    showSpinner : function (component, event, helper) {
        //console.log("show spinner");
        var spinner = component.find("waitSpinner");
        $A.util.removeClass(spinner, "slds-hide");
    },
    
    hideSpinner : function (component, event, helper) {
        //console.log("hide spinner");
        var spinner = component.find("waitSpinner");
        $A.util.addClass(spinner, "slds-hide");
    }    
})